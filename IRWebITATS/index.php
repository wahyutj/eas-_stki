<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>WebIRSkripsi</title>
    <meta name="generator" content="WYSIWYG Web Builder 11 - http://www.wysiwygwebbuilder.com">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="InfromationRetrievalWeb.css" rel="stylesheet">
    <link href="index.css" rel="stylesheet">
  </head>
  <body>
    <div id="container">
    </div>
    <div id="wb_LayoutGrid1">
      <div id="LayoutGrid1">
        <div class="row">
          <div class="col-1">
            <div id="wb_LayoutGrid2">
              <div id="LayoutGrid2">
                <div class="row">
                  <div class="col-1">
                  <hr id="Line3">
                  </div>
                  <form action="index.php" method="post">
                      <div class="col-2">
                        <hr id="Line4">
                        <div id="wb_Text1">
                          <span style="color:#FFFFFF;font-family:Stencil;font-size:37px;">Information Retrieval <br>Skripsi ITATS</span>
                        </div>
                        <hr id="Line2">
                        <input type="text" id="Editbox1" name="keyword" value="">
                        <hr id="Line5">
                        <input type="submit" id="Button2" name="" value="Search">
                        <hr id="Line1">
                      </div>
                  </form>
                  <div class="col-3">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div style="font-size:10px; font-family:comicsans">
    <?php
      include 'conn.php';
      include 'function.php';

      // VIEW ALL DATASET FROM tbskripsi (corpus)
      // print("<hr />");
      // $result = mysqli_query($con,"SELECT * FROM tbskripsi ORDER BY Id");
      // while($row = mysqli_fetch_assoc($result)) {
      //      echo "<font color =brown>" . $row['Id'] . ". ". $row['Judul'] . "</font><br />" . $row['Abstrak'];
      //      echo "<hr />";
      // }

      // INDEXING PROCCESS
      // print("<hr />");
	    // buatindex();
      // print("Indexing Succesfully");
      // print("<hr />");

      // CALCULATING WEIGHT
      // print("<hr />");
      // hitungbobot();
      // print("<hr />");

      // LENGTH VERKTOR
      // print("<hr />");
      // panjangvektor();
      // print("<hr />");

      // VIEW VERKTOR
      // print("<")
      // print("<hr />");
      // print("<table>");
      // print("<tr><td>Doc-ID</td><td>Vector Length</td></tr>");
      // $result = mysqli_query($con,"SELECT * FROM tbvektor");
      //
      // while($row = mysqli_fetch_assoc($result)) {
      //
      //       print("<tr>");
      //       print("<td>" . $row['DocId'] . "</td><td>" . $row['Panjang'] . "</td>");
      //       print("</tr>");
      //
      // }
      // print("</table><hr />");

      // VIEW INDEX
      // print("<hr />");
      // print("<table>");
      // print("<tr><td>#</td><td>Term</td><td>Doc-ID</td><td>Count</td><td>Weight</td></tr>");
      // $result = mysqli_query($con,"SELECT * FROM tbindex ORDER BY Id");
      //
      // while($row = mysqli_fetch_assoc($result)) {
      //
      //       print("<tr>");
      //       print("<td>" . $row['Id'] . "</td><td>" . $row['Term'] . "</td><td>" . $row['DocId'] .
      //                 "</td><td>" . $row['Count'] . "</td><td>" . $row['Bobot'] . "</td>");
      //       print("</tr>");
      //
      // }
      // print("</table><hr />");
      // print("<hr />");

      // RETRIEVE INFORMATION FROM QUERY
      @$keyword = $_POST["keyword"];

      if ($keyword)  {
            print("<hr />");
            $keyword = preproses($keyword);
            print('Hasil retrieval untuk <font color=brown><b>' . $_POST["keyword"]  . '</b></font> (<font color=brown><b>' . $keyword . '</b></font>) adalah <hr />');
            ambilcache($keyword);
            //hitungsim($keyword);
      }

      // VIEW CACHE
      // print("<table>");
      // print("<tr><td>#</td><td>Query</td><td>Doc-ID</td><td>Value</td></tr>");
      // $result = mysqli_query($con,"SELECT * FROM tbcache ORDER BY Query ASC");
      //
      // while($row = mysqli_fetch_assoc($result)) {
      //
      //       print("<tr>");
      //       print("<td>" . $row['Id'] . "</td><td>" . $row['Query'] . "</td><td>" . $row['DocId'] .
      //                 "</td><td>" . $row['Value'] . "</td>");
      //       print("</tr>");
      //
      // }
      // print("</table><hr />");
    ?>
   </div>
    <div id="wb_LayoutGrid4">
      <div id="LayoutGrid4">
        <div class="row">
          <div class="col-1">
            <div id="wb_LayoutGrid5">
              <div id="LayoutGrid5">
                <div class="row">
                  <div class="col-1">
                  <hr id="Line6">
                  <div id="wb_Text2">
                    <span style="color:#FFFFFF;font-family:'Comic Sans MS';font-size:13px;">Copyright @ Ied Andro Medha - Aries Aprilia - Wahyu Tj</span>
                  </div>
                  <hr id="Line7">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
