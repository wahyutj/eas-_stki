<?php

//=============== koleksi fungsi ===================
//fungsi untuk melakukan preprocessing terhadap teks
//terutama stopword removal dan stemming
//--------------------------------------------------------------------------------------------
function preproses($teks) {
      include "conn.php";

       //bersihkan tanda baca, ganti dengan space
       $teks = str_replace("'", " ", $teks);
       $teks = str_replace("-", " ", $teks);
       $teks = str_replace(")", " ", $teks);
       $teks = str_replace("(", " ", $teks);
       $teks = str_replace("\"", " ", $teks);
       $teks = str_replace("/", " ", $teks);
       $teks = str_replace("=", " ", $teks);
       $teks = str_replace(".", " ", $teks);
       $teks = str_replace(",", " ", $teks);
       $teks = str_replace(":", " ", $teks);
       $teks = str_replace(";", " ", $teks);
       $teks = str_replace("!", " ", $teks);
       $teks = str_replace("?", " ", $teks);

       //ubah ke huruf kecil
       $teks = strtolower(trim($teks));

       //terapkan stop word removal
       $astoplist = array ("yang","ada", "adanya", "adalah", "adapun", "agak", "agaknya", "agar", "akan", "akankah", "akhirnya", "aku", "akulah", "amat", "amatlah", "anda", "andalah", "antar
", "diantaranya", "antara", "antaranya", "diantara", "apa", "apaan", "mengapa", "apabila", "apakah", "apalagi", "apatah", "atau", "ataukah", "ataupun", "bagai
", "bagaikan", "sebagai", "sebagainya", "bagaimana", "bagaimanapun", "sebagaimana", "bagaimanakah", "bagi", "bahkan", "bahwa", "bahwasanya", "sebaliknya", "banyak
", "sebanyak", "beberapa", "seberapa", "begini", "beginian", "beginikah", "beginilah", "sebegini", "begitu", "begitukah", "begitulah", "begitupun", "sebegitu", "belum
", "belumlah", "sebelum", "sebelumnya", "sebenarnya", "berapa", "berapakah", "berapalah", "berapapun", "betulkah", "sebetulnya", "biasa", "biasanya", "bila", "bilakah
", "bisa", "bisakah", "sebisanya", "boleh", "bolehkah", "bolehlah", "buat", "bukan", "bukankah", "bukanlah", "bukannya", "cuma", "percuma", "dahulu", "dalam", "dan
", "dapat", "dari", "daripada", "dekat", "demi", "demikian", "demikianlah", "sedemikian", "dengan", "depan", "di", "dia", "dialah", "dini", "diri", "dirinya", "terdiri
", "dong", "dulu", "enggak", "enggaknya", "entah", "entahlah", "terhadap", "terhadapnya", "hal", "hampir", "hanya", "hanyalah", "harus", "haruslah", "harusnya
", "seharusnya", "hendak", "hendaklah", "hendaknya", "hingga", "sehingga", "ia", "ialah", "ibarat", "ingin", "inginkah", "inginkan", "ini", "inikah", "inilah", "itu
", "itukah", "itulah", "jangan", "jangankan", "janganlah", "jika", "jikalau", "juga", "justru", "kala", "kalau", "kalaulah", "kalaupun", "kalian", "kami", "kamilah
", "kamu", "kamulah", "kan", "kapan", "kapankah", "kapanpun", "dikarenakan", "karena", "karenanya", "ke", "kecil", "kemudian", "kenapa", "kepada", "kepadanya
", "ketika", "seketika", "khususnya", "kini", "kinilah", "kiranya", "sekiranya", "kita", "kitalah", "kok", "lagi", "lagian", "selagi", "lah", "lain", "lainnya
", "melainkan", "selaku", "lalu", "melalui", "terlalu", "lama", "lamanya", "selama", "selama", "selamanya", "lebih", "terlebih", "bermacam", "macam", "semacam", "maka
", "makanya", "makin", "malah", "malahan", "mampu", "mampukah", "mana", "manakala", "manalagi", "masih", "masihkah", "semasih", "masing", "mau", "maupun", "semaunya
", "memang", "mereka", "merekalah", "meski", "meskipun", "semula", "mungkin", "mungkinkah", "nah", "namun", "nanti", "nantinya", "nyaris", "oleh", "olehnya", "seorang
", "seseorang", "pada", "padanya", "padahal", "paling", "sepanjang", "pantas", "sepantasnya", "sepantasnyalah", "para", "pasti", "pastilah", "per", "pernah", "pula", "pun
", "merupakan", "rupanya", "serupa", "saat", "saatnya", "sesaat", "saja", "sajalah", "saling", "bersama", "sama", "sesama", "sambil", "sampai", "sana", "sangat
", "sangatlah", "saya", "sayalah", "se", "sebab", "sebabnya", "sebuah", "tersebut", "tersebutlah", "sedang", "sedangkan", "sedikit", "sedikitnya", "segala", "segalanya", "segera
", "sesegera", "sejak", "sejenak", "sekali", "sekalian", "sekalipun", "sesekali", "sekaligus", "sekarang", "sekarang", "sekitar", "sekitarnya", "sela", "selain", "selalu", "seluruh
", "seluruhnya", "semakin", "sementara", "sempat");

	   foreach ($astoplist as $i => $value) {
			$teks = str_replace($astoplist[$i], '', $teks);
	   }

       //terapkan stemming
       //buka tabel tbstem dan bandingkan dengan berita
       $restem = mysqli_query($con,"SELECT * FROM tbstem ORDER BY Id");

       while($rowstem = mysqli_fetch_assoc($restem)) {
              $teks = str_replace($rowstem['Term'], $rowstem['Stem'], $teks);
       }

       //kembalikan teks yang telah dipreproses
       $teks = strtolower(trim($teks));
       return $teks;
} //end function preproses
//--------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------
//fungsi untuk membuat index
function buatindex() {
        include "conn.php";
        set_time_limit(60);

        //hapus index sebelumnya
        mysqli_query($con,"TRUNCATE TABLE tbindex");

        //ambil semua berita (teks)
        $resAbstrak = mysqli_query($con,"SELECT * FROM tbskripsi ORDER BY Id");
        $num_rows = mysqli_num_rows($resAbstrak);
        //print("Mengindeks sebanyak " . $num_rows . " berita. <br />");

        while($row = mysqli_fetch_assoc($resAbstrak)) {
               $docId = $row['Id'];
               $Abstrak = $row['Abstrak'];

               //terapkan preprocessing
               $Abstrak = preproses($Abstrak);

               //simpan ke inverted index (tbindex)
               $aAbstrak = explode(" ", trim($Abstrak));

               foreach ($aAbstrak as $j => $value) {
                     //hanya jika Term tidak null atau nil, tidak kosong
                     if ($aAbstrak[$j] != "") {

                            //berapa baris hasil yang dikembalikan query tersebut?
                            $rescount = mysqli_query($con,"SELECT Count FROM tbindex WHERE Term = '$aAbstrak[$j]' AND DocId = $docId");
                            $num_rows = mysqli_num_rows($rescount);

                            //jika sudah ada DocId dan Term tersebut , naikkan Count (+1)
                            if ($num_rows > 0) {
                                   $rowcount = mysqli_fetch_assoc($rescount);
                                   $count = $rowcount['Count'];
                                   $count++;

                                   mysqli_query($con,"UPDATE tbindex SET Count = $count WHERE Term = '$aAbstrak[$j]' AND DocId = $docId");
                            }
                            //jika belum ada, langsung simpan ke tbindex
                            else {
                                   mysqli_query($con,"INSERT INTO tbindex (Term, DocId, Count) VALUES ('$aAbstrak[$j]', $docId, 1)");
                            }
                     } //end if
               } //end foreach
        } //end while
} //end function buatindex()
//--------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------
//fungsi hitungbobot, menggunakan pendekatan tf.idf
function hitungbobot() {
        include "conn.php";
       //berapa jumlah DocId total?, n
       $resn = mysqli_query($con,"SELECT DISTINCT DocId FROM tbindex");
       $n = mysqli_num_rows($resn);

       //ambil setiap record dalam tabel tbindex
       //hitung bobot untuk setiap Term dalam setiap DocId
       $resBobot = mysqli_query($con,"SELECT * FROM tbindex ORDER BY Id");
       $num_rows = mysqli_num_rows($resBobot);
       print("There are " . $num_rows . " Term given weights. <br />");

       while($rowbobot = mysqli_fetch_assoc($resBobot)) {
              //$w = tf * log (n/N)
              $term = $rowbobot['Term'];
              $tf = $rowbobot['Count'];
              $id = $rowbobot['Id'];

              //berapa jumlah dokumen yang mengandung term tersebut?, N
              $resNTerm = mysqli_query($con,"SELECT Count(*) as N FROM tbindex WHERE Term = '$term'");
              $rowNTerm = mysqli_fetch_assoc($resNTerm);
              $NTerm = $rowNTerm['N'];

              $w = $tf * log($n/$NTerm);

              //update bobot dari term tersebut
              $resUpdateBobot = mysqli_query($con,"UPDATE tbindex SET Bobot = $w WHERE Id = $id");
       } //end while $rowbobot
} //end function hitungbobot
//--------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------
//fungsi panjangvektor, jarak euclidean
//akar(penjumlahan kuadrat dari bobot setiap Term)
function panjangvektor() {
      include "conn.php";
       //hapus isi tabel tbvektor
       mysqli_query($con,"TRUNCATE TABLE tbvektor");

       //ambil setiap DocId dalam tbindex
       //hitung panjang vektor untuk setiap DocId tersebut
       //simpan ke dalam tabel tbvektor
       $resDocId = mysqli_query($con,"SELECT DISTINCT DocId FROM tbindex");

       $num_rows = mysqli_num_rows($resDocId);
       print("There are " . $num_rows . " documents that the vector length is calculated. <br />");

       while($rowDocId = mysqli_fetch_assoc($resDocId)) {
              $docId = $rowDocId['DocId'];

              $resVektor = mysqli_query($con,"SELECT Bobot FROM tbindex WHERE
DocId = $docId");

              //jumlahkan semua bobot kuadrat
              $panjangVektor = 0;
              while($rowVektor = mysqli_fetch_assoc($resVektor)) {
                     $panjangVektor = $panjangVektor + $rowVektor['Bobot']  *  $rowVektor['Bobot'];
              }

              //hitung akarnya
              $panjangVektor = sqrt($panjangVektor);

              //masukkan ke dalam tbvektor
              $resInsertVektor = mysqli_query($con,"INSERT INTO tbvektor (DocId,
Panjang) VALUES ($docId, $panjangVektor)");
       } //end while $rowDocId
} //end function panjangvektor
//--------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------
//fungsi hitungsim - kemiripan antara query
//setiap dokumen dalam database (berdasarkan bobot di tbindex)
function hitungsim($query) {
      include "conn.php";
       //ambil jumlah total dokumen yang telah diindex (tbindex atau tbvektor), n
       $resn = mysqli_query($con,"SELECT Count(*) as n FROM tbvektor");
       $rown = mysqli_fetch_assoc($resn);
       $n = $rown['n'];

       //terapkan preprocessing terhadap $query
       $aquery = explode(" ", $query);

       //hitung panjang vektor query
       $panjangQuery = 0;
       $aBobotQuery = array();

       for ($i=0; $i<count($aquery); $i++) {
              //hitung bobot untuk term ke-i pada query, log(n/N);
              //hitung jumlah dokumen yang mengandung term tersebut
              $resNTerm = mysqli_query($con,"SELECT Count(*) as N from tbindex WHERE Term = '$aquery[$i]'");
              $rowNTerm = mysqli_fetch_assoc($resNTerm);
              $NTerm = $rowNTerm['N'] ;

              $idf = log($n/$NTerm);

              // CONTOH LAIN UNTUK MENGAKALI NOTICE TIDAK ADA
              // $idf = log($NTerm/$n);

              //simpan di array
              $aBobotQuery[] = $idf;

              $panjangQuery = $panjangQuery + $idf * $idf;
       }

       $panjangQuery = sqrt($panjangQuery);

       $jumlahmirip = 0;

       //ambil setiap term dari DocId, bandingkan dengan Query
       $resDocId = mysqli_query($con,"SELECT * FROM tbvektor ORDER BY DocId");
       while ($rowDocId = mysqli_fetch_assoc($resDocId)) {

              $dotproduct = 0;

              $docId = $rowDocId['DocId'];
              $panjangDocId = $rowDocId['Panjang'];

              $resTerm = mysqli_query($con,"SELECT * FROM tbindex WHERE DocId
= $docId");
              while ($rowTerm = mysqli_fetch_assoc($resTerm)) {
                     for ($i=0; $i<count($aquery); $i++) {
                           //jika term sama
                           if ($rowTerm['Term'] == $aquery[$i]) {
                                  $dotproduct = $dotproduct + $rowTerm['Bobot'] * $aBobotQuery[$i];
                           } //end if
                     } //end for $i
              } //end while ($rowTerm)

              if ($dotproduct > 0) {
                     $sim = $dotproduct / ($panjangQuery * $panjangDocId);

                     //simpan kemiripan > 0  ke dalam tbcache
                     $resInsertCache = mysqli_query($con,"INSERT INTO tbcache (Query,
DocId, Value) VALUES ('$query', $docId, $sim)");
                     $jumlahmirip++;
              }

       } //end while $rowDocId

       if ($jumlahmirip == 0) {
              $resInsertCache = mysqli_query($con,"INSERT INTO tbcache (Query,
DocId, Value) VALUES ('$query', 0, 0)");
       }

} //end hitungSim()
//--------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------
function ambilcache($keyword) {
  include ("conn.php");
       $resCache = mysqli_query($con,"SELECT *  FROM tbcache WHERE
Query = '$keyword' ORDER BY Value DESC");
       $num_rows = mysqli_num_rows($resCache);

       if ($num_rows >0) {
              //tampilkan semua berita yang telah terurut
              while ($rowCache = mysqli_fetch_array($resCache)) {
                     $docId = $rowCache['DocId'];
                     $sim = $rowCache['Value'];

                     if ($docId != 0) {
                           //ambil berita dari tabel tbskripsi, tampilkan
                           $resAbstrak = mysqli_query($con,"SELECT * FROM tbskripsi
WHERE Id = $docId");
                           $rowAbstrak = mysqli_fetch_array($resAbstrak);

                           $judul = $rowAbstrak['Judul'];
                           $abstrak = $rowAbstrak['Abstrak'];
                           print($docId . ". (" . $sim . ") <font color=brown><b>" . $judul . "</b></font><br />");
                           print($abstrak . "<hr />");
                     } else {
                           print("<b>Tidak ada... </b><hr />");
                     }
              }//end while (rowCache = mysql_fetch_array($resCache))
       }//end if $num_rows>0
       else {
              hitungsim($keyword);

              //pasti telah ada dalam tbcache
              $resCache = mysqli_query($con,"SELECT * FROM tbcache WHERE
Query = '$keyword' ORDER BY Value DESC");
              $num_rows = mysqli_num_rows($resCache);

              while ($rowCache = mysqli_fetch_array($resCache)) {
                     $docId = $rowCache['DocId'];
                     $sim = $rowCache['Value'];

                     if ($docId != 0) {
                           //ambil berita dari tabel tbskripsi, tampilkan
                           $resAbstrak = mysqli_query($con,"SELECT * FROM tbskripsi
 WHERE Id = $docId");
                           $rowAbstrak = mysqli_fetch_array($resAbstrak);

                           $judul = $rowAbstrak['Judul'];
                           $abstrak = $rowAbstrak['Abstrak'];

                           print($docId . ". (" . $sim . ") <font color=brown><b>" . $judul . "</b></font><br />");
                           print($abstrak . "<hr />");
                     } else {
                           print("<b>Tidak ada... </b><hr />");
                     }
              } //end while
       }
} //end function ambilcache
//--------------------------------------------------------------------------------------------
//============== akhir koleksi fungsi ==================

?>
